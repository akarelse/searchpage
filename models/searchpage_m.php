<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* this file is part of a searchpage module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class searchpage_m extends MY_Model {

	public function __construct()
	{	
		parent::__construct();
		//$this->_table = 'searchpage';
	}
	
	/**
	* get the pages and their chunks
	* @access public
	* @param searchterm
	* @return array
	*/
	public function getpageresults($search,$limit)
	{
		$this->db->distinct();
		$this->db->group_by("title");
		$this->db->select('title,default_pages.slug,default_page_chunks.body');
		$this->db->where('MATCH (default_page_chunks.body,default_pages.title) AGAINST ("' . $search . '" IN BOOLEAN MODE)', NULL, FALSE);
		$this->db->where("default_pages.status","live");
		$this->db->from('default_pages');
		$this->db->join('default_page_chunks', 'default_pages.id = default_page_chunks.page_id');
		$this->db->limit($limit);
		$query = $this->db->get();
		return $query->result();
	}
	
	
	/**
	* getting the blog items
	* @access public
	* @param searchterm
	* @return array
	*/
	public function getblogresults($search,$limit)
	{
		$this->db->distinct();
		$this->db->group_by("default_blog.title");		
		$this->db->select('default_blog.title,default_blog.slug,default_blog.created_on,default_blog.body');
		$this->db->where('MATCH (default_blog.body,default_blog.title,default_blog.intro) AGAINST ("' . $search . '" IN BOOLEAN MODE)', NULL, FALSE);
		$this->db->where("default_blog.status","live");
		$this->db->from('default_blog');
		$this->db->limit($limit);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getcommentresults($search,$limit)
	{
		$this->db->distinct();
		$this->db->group_by("default_blog.id");
		$this->db->select("comment as body,module,module_id,default_blog.slug,default_blog.created_on,default_blog.title");
		$this->db->where('MATCH (name,comment,website) AGAINST ("' . $search . '" IN BOOLEAN MODE)', NULL, FALSE);
		$this->db->where("is_active","1");
		$this->db->from('default_comments');
		$this->db->join('default_blog', 'default_comments.module_id = default_blog.id');
		$this->db->limit($limit);
		$query = $this->db->get();
		return $query->result();	
	}

}
?>
