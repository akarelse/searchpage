$(document).ready(function(){
  $('.search').each(function(){
    var defaultVal = $(this).attr('title');
    $(this).focus(function(){
      if ($(this).val() == defaultVal){
        $(this).removeClass('active').val('');
      }
    })
    .blur(function(){
      if ($(this).val() == ''){
        $(this).addClass('active').val(defaultVal);
      }
    })
    .blur().addClass('active');
  });
  $('form').submit(function(){
    $('.search').each(function(){
      var defaultVal = $(this).attr('title');
      if ($(this).val() == defaultVal){
        $(this).val('');
      }
    });
  });
});
