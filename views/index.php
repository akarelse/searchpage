	{{ if items_exist == false }}

		<p>{{helper:lang line="searchpage:no_items"}}</p>
	{{ else }}
			<ul>
				{{ items }}
				<li>{{ url:anchor segments=slug title=title}}<br>{{ body }}</li>
				{{ /items }}
			</ul>
		{{ pagination:links }}
		
	{{ endif }}

	

