<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* this file is part of a searchpage module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a searchpage module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Searchpage Module
 */


class Module_SearchPage extends Module {

	public $version = '1.0.0';
	const MIN_PHP_VERSION = '5.3.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'SearchPage'
			),
			'description' => array(
				'en' => 'Search your pages,blog and comments',
				'nl' => 'Doorzoek je paginas, blog en comments'
			),
			'frontend' => TRUE,
			'backend' => FALSE,
			'menu' => 'content' 
			
		);
	}

	public function install()
	{
		
		if (!$this->check_php_version())
		{
			return FALSE;
		}
		
		if ($this->settingsexist())
		{
			$this->update();
		}
			
		$this->db->trans_start();
		
				
		
			$lengthofbody = array(
			'slug' => 'searchpage_length_setting',
			'title' => 'Searchpage Setting',
			'description' => 'The max length of the search result item body.',
			'`default`' => '1',
			'`value`' => '50',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'searchpage'
		);
		
			$searchblog = array(
			'slug' => 'searchpage_blog_setting',
			'title' => 'Search blog ?',
			'description' => 'Select yes to search the blog.',
			'`default`' => '1',
			'`value`' => '1',
			'type' => 'select',
			'`options`' => '1=Yes|0=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'searchpage'
		);
		
			$searchpages = array(
			'slug' => 'searchpage_pages_setting',
			'title' => 'Search pages ?',
			'description' => 'Select yes to search the normal pages.',
			'`default`' => '1',
			'`value`' => '1',
			'type' => 'select',
			'`options`' => '1=Yes|0=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'searchpage'
		);
		
		$searchcomments = array(
			'slug' => 'searchpage_comment_setting',
			'title' => 'Search comments ?',
			'description' => 'Select yes to search the comments pages.',
			'`default`' => '1',
			'`value`' => '1',
			'type' => 'select',
			'`options`' => '1=Yes|0=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'searchpage'
		);
		
			$searchtermlength = array(
			'slug' => 'searchpage_termlength_setting',
			'title' => 'Searchterm length',
			'description' => 'Minimal length of searchterm.',
			'`default`' => '1',
			'`value`' => '2',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'searchpage'
		);
		
		$searchpagination = array(
			'slug' => 'searchpage_pagination_setting',
			'title' => 'Seach pagination',
			'description' => 'Nr of search results before pagination.',
			'`default`' => '1',
			'`value`' => '20',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'searchpage'
		);
		
		$this->db->query("ALTER TABLE  `default_page_chunks` ADD FULLTEXT (`body`)");
		
		$this->db->query("ALTER TABLE  `default_blog` ADD FULLTEXT (`body`)");
		$this->db->query("ALTER TABLE  `default_blog` ADD FULLTEXT (`title`)");
		$this->db->query("ALTER TABLE  `default_blog` ADD FULLTEXT (`intro`)");
		$this->db->query("ALTER TABLE  `default_pages` ADD FULLTEXT (`title`)");
		
		$this->db->query("ALTER TABLE  `default_comments` ADD FULLTEXT (`comment`)");
		$this->db->query("ALTER TABLE  `default_comments` ADD FULLTEXT (`name`)");

		$this->db->insert('settings', $lengthofbody);
		$this->db->insert('settings', $searchblog);
		$this->db->insert('settings', $searchpages);
		$this->db->insert('settings', $searchcomments);
		$this->db->insert('settings', $searchtermlength);
		$this->db->insert('settings', $searchpagination);
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	public function uninstall()
	{
	if ($this->settingsexist())
	{
		if ($this->db->delete('settings', array('module' => 'searchpage')))
		{
			return TRUE;
		}
		return FALSE;
	}
	return TRUE;

		
		
	}


	public function upgrade($old_version)
	{
			if ($this->uninstall() && $this->install() )
			return TRUE;
			else
			return FALSE;
	}

	public function help()
	{
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	/**
	 * Check the current version of PHP and thow an error if it's not good enough
	 *
	 * @access private
	 * @return boolean
	 * @author Victor Michnowicz
	 */
	private function check_php_version()
	{
		// If current version of PHP is not up snuff
		if ( version_compare(PHP_VERSION, self::MIN_PHP_VERSION) < 0 )
		{
			show_error('This addon requires PHP version ' . self::MIN_PHP_VERSION . ' or higher.');
      return FALSE;
		}
    return TRUE;
	}
	
	private function settingsexist()
	{
	
	$this->db->where("module","searchpage");
	$this->db->from('settings');
	if ($this->db->count_all_results() > 0)
	{
		return true;
	}
	return false;	
	}
	
	
}
/* End of file details.php */
