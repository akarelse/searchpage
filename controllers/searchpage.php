<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* this file is part of a searchpage module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a searchpage module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Searchpage Module
 */
 
class searchpage extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
		// Load the required classes and language
		$this->load->model('searchpage_m');
		$this->lang->load('searchpage');
	}

	/**
	 * All items
	 */
	public function searchurl()
	{

		// of $this->uri->uri_to_assoc(3);
		$searchterm = $this->uri->segment(2, 0); //FOR RIGHT SEARCH
		$pagina = $this->uri->segment(3, 1);
		$data = $this->displayresults($searchterm,$pagina);
		$data->pagination = create_pagination('searchpage/'. $searchterm, count($data->items)+1, $this->settings->searchpage_pagination_setting , 3);
		$this->template->title(lang('searchpage:indextitle'))
		->set_breadcrumb(lang('searchpage:indextitle'))
		->build('index', $data);
	}
	
	public function searchget()
	{
		$searchterm = $this->input->get('search');
		redirect('searchpage/' . $searchterm); 
	}
	
	private function displayresults($search,$page = 0)
	{
		
		$rawpagedata = array();
		$rawblogdata = array();
		$rawcommentdata = array();
		
		
		if (strlen($search) > $this->settings->searchpage_termlength_setting)
		{
			
			if ($this->settings->searchpage_pages_setting)
			{
				$rawpagedata = $this->searchpage_m->getpageresults($search,$this->settings->searchpage_pagination_setting);
			}
			
			if ($this->settings->searchpage_blog_setting)
			{
				$rawblogdata = $this->searchpage_m->getblogresults($search,$this->settings->searchpage_pagination_setting);
				$rawblogdata = $this->rightpath($rawblogdata);
			}
			
			if ($this->settings->searchpage_comment_setting)
			{
				$rawcommentdata = $this->searchpage_m->getcommentresults($search,$this->settings->searchpage_pagination_setting);
				$rawcommentdata = $this->rightpath($rawcommentdata);
			}
			
	}
		
		$tempar = array_merge($rawblogdata,$rawpagedata,$rawcommentdata);
		
		$tempar = array_slice($tempar, ($page-1)*$this->settings->searchpage_pagination_setting,$this->settings->searchpage_pagination_setting);
		// stripping html and limitim the body size
		
		
		$data->items = $this->strip_body($tempar,'body');
		$data->items = $this->limit_body($data->items,$this->settings->searchpage_length_setting,' ...','body');	
		// we'll do a quick check here so we can tell tags whether there is data or not
		if (count($data->items))
		{
			$data->items_exist = TRUE;
		}
		else
		{
			$data->items_exist = FALSE;
		}	
		// we're using the pagination helper to do the pagination for us. Params are: (module/method, total count, limit, uri segment)

		return $data;
	}
	
	private function last_filter($allitems, $search)
	{
		$tempar = array();
		foreach($allitems as $k => $v)
		{
			$dontadd = 0;
			$v = $this->objectToArray($v);
			foreach($v as $key => $value)
			{
				
				if($key == 'body')
				{
					if (strripos($value,$search) === FALSE)
					{
						$dontadd = 1;
					}
				}
			}
			if ($dontadd == 0)
			{
				$v = $this->arrayToObject($v);
				$tempar[] = $v;
			}
		}
		return $tempar;	// the dirty mind break magic, GRANDMA.
	}
	
	/**
	* making the date string from a timestamp to the url of a blog thingy
	* @access private
	* @param array
	* @return array
	*/
	private function rightpath($allitems)
	{
	$tempar = array();
		foreach($allitems as $k => $v)
		{
			$v->slug = "blog/" . date('Y',$v->created_on) . "/" . date('d',$v->created_on) . "/" . $v->slug;
			$tempar[] = $v;
		}
		return $tempar;
	}
	
	/**
	* limit the body of the search item, dont want one page searchitems.
	* @access private
	* @param array
	* @param int
	* @param string
	* @return array
	*/
	private function limit_body($allitems,$length,$endstring,$property){	
	$tempar = array();
		foreach($allitems as $k => $v)
		{
				$v->{$property} = word_limiter($v->{$property}, $length);
				$tempar[] = $v;
		}
		return $tempar;
	}
	
	/**
	* and yes it is all html so that needs to be stripped.
	* @access private
	* @param array
	* @return array
	*/
	private function strip_body($allitems, $property) // the dirty mind break magic, GRANDMA.
	{
		$tempar = array();
		foreach($allitems as $k => $v)
		{
			$v->{$property} = strip_tags($v->{$property});
			$v->{$property} = preg_replace('#({{).*?(}})#',"",$v->{$property});
			$tempar[] = $v;
		}
		return $tempar;	
	}
	

	

	
}


?>
