<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* this file is part of a searchpage module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Plugin_searchpage extends Plugin
{

	private $modulename = 'searchpage';
	private $themepath;
	private $modulepath;
	private $data;
	
	public function __construct()
	{
		$this->load->helper('file');
		$this->modulename = 'searchpage';
		$this->themepath = $this->load->get_var('template_views') . 'modules/' . $this->modulename . '/';
		
		if (get_file_info( ADDONPATH .'modules/' . $this->modulename . '/details.php' ))
		{
			$this->modulepath= ADDONPATH .'modules/' . $this->modulename;
		}
		else
		{
			$this->modulepath= SHARED_ADDONPATH .'modules/' . $this->modulename;
		}
	}
	
	function searchfield()
	{
		$data = $this->load->get_vars();
		$this->lang->load($this->modulename. '/' . $this->modulename);
		
		
		if (get_file_info( $this->themepath ))
		{
			$string = $this->load->file($this->themepath . 'partials/searchfield.html',TRUE,TRUE);
			return $this->parser->parse_string($string, $data, TRUE, TRUE);
			
		}
		else
		{
			$string = $this->load->file($this->themepath . 'partials/searchfield.html',TRUE,TRUE);
			return $this->parser->parse_string($string, $data, TRUE, TRUE);
		}
	}
	
	function defsearchtxt()
	{
		$string = $this->load->file($this->themepath . 'js/defsearchtxt.js',TRUE,TRUE);
		return $string;
	}
	
	function searchurl()
	{
		$string = $this->load->file($this->modulepath . '/js/maketourl.js',TRUE,TRUE);
		$data->modulename = $this->modulename;
		return $this->parser->parse_string('<script>' . $string . '</script>', $data, TRUE, TRUE);
	}
}
