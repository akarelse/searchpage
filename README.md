# Searchpage for PyroCMS

## Legal

This module was originally written by [Aat Karelse](http://vuurrosmedia.nl)
It is licensed under the version 3 of the[GPL license](http://www.gnu.org/licenses/)
More information about this module can be found at [modules-vuurrosmedia](http://modules.vuurrosmedia.nl)

---

## Overview

Searchpage is a back-end module for PyroCMS and it supports the latest version 2.1.x 
It gives visitors the option to search the pages, blog and comments.

---

## Features

- search the pages, blog and comments.
- gives the admin the option to disable blog searching.
- gives the admin the option to disable comments searching.
- gives the admin the option to disable page searching.
- gives the admin the option to alter the length of the search results.
- gives the admin the option to alter the minimal length of the search term.

---

## Installation

Download the archive and upload via Control Panel or Download the module from bitbucket.
Install the module.

---

## Usage

    {{ searchpage:searchfield }}

---

## Credits

- Jerel Unruh for making available the sample module.
- The Pyro Dev team for making PyroCMS

## ToDo

+ Better languages
+ Translation
+ Integration of searchfield clear on click script direct in searchpages field widget
+ Making sure the admin add edit delete are in color ?
+ Better indication that search term is too short.